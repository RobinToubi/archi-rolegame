import { Character } from './Character.js';
import { Reward } from './Reward.js';

export class Monster extends Character {
    reward: Reward;
    
    relatedQuest = new Array();

    constructor(id: number, pv: number, attack: number, pseudo: string, spd: number, reward: Reward) {
        super();
        this.id = id;
        this.atq = attack;
        this.hp = pv;
        this.pseudo = pseudo;
        this.speed = spd;
        this.alive = true;
        this.reward = reward;
    }
}