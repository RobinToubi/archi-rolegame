export class Item {
    id:number;
    name:string;
    atkBonus:number;
    hpBonus:number;

    constructor(id: number,name: string,atk: number,hp: number) {
        this.id = id;
        this.name = name;
        this.atkBonus = atk;
        this.hpBonus = hp;
    }
}