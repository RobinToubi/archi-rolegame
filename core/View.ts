import 'path';
import { environment } from './Environment';

abstract class View {
    name: string;

    constructor() {

    }

    render() {
        document.get = environment.path + this.name;
    }
}