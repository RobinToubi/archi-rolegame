import { Effect } from "./Effect.js";

export abstract class Character {
  id: number;
  pseudo: string;
  hp: number;
  atq: number;
  speed: number;
  alive: boolean;
  state = new Array<Effect>();

  addEffect(effect: Effect) {
    this.state.push(effect);
  }

  removeEffect(effect: Effect) {
    let id = this.state.indexOf(effect);
    if (id === -1) {
      console.error("Pas d'effet trouvé");
      return;
    }
    this.state.splice(id, 1);
  }

  attack(target: Character) {
    target.hp -= this.atq;
  }
}
