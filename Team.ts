import { Player } from './Player.js';
import { PlayerIterator } from './PlayerIterator.js';
import { Reward } from './Reward.js';

export class Team {
    players:Array<Player>;
    name:string;
    questOnStage: string;
    playerIterator: PlayerIterator;

    constructor(players: Array<Player>, name:string) {
        this.players = players;
        this.name = name;
        this.playerIterator = new PlayerIterator(players);
    }

    getReward(reward: Reward) {
        while (this.playerIterator.valid()) {
            this.playerIterator.current().getReward(reward);
        }
    }

    getPlayers() {
        return this.players;
    }

    addPlayer(pl : Player) {
        this.players.push(pl);
    }
}