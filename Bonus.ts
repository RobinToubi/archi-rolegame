import { Effect } from "./Effect.js";
import { Character } from './Character.js';
import { Type } from "./TypeEffect.js";

export class Bonus extends Effect {
    hpBonus:number;
    typeBonus: Type;

    constructor(type:Type, duration:number, character: Character) {
        super(duration, character);
        this.typeBonus = type;
    }
}