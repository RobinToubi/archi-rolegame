export interface IPlayerIterator<Player> {
    current(): Player;
    next(): Player;
    key(): Player;
    alive(): boolean;
    valid(): boolean;
}

