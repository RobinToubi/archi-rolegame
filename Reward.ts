import { Item } from './Item.js';

export class Reward {
    xp:number;
    items: Array<Item>;
    gold: number;

    constructor(xp:number, items:Array<Item>, gold:number) {
        this.gold = gold;
        this.xp = xp;
        this.items = items || null;
    }
}