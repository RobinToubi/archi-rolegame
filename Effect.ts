import { Character } from './Character.js';

export abstract class Effect {
  duration: number;
  player: Character;

  constructor(duration: number, player: Character) {
    this.duration = duration;
    this.player = player;
  }

  
}