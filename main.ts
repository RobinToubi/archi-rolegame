import { Fight } from "./Fight.js";
import { Monster } from "./Monster.js";
import { Player } from "./Player.js";
import { Reward } from "./Reward.js";
import { Team } from "./Team.js";
import { Item } from './Item.js';

console.log('toto')
var team = new Team(new Array(), "team1");
var player = new Player(1, 20, 10, "Robin", 2, team);
var item = new Item(1, "ITEM", 12,12);
var reward = new Reward(4, new Array(item), 5);
var monster = new Monster(1, 20, 2, "BOSS", 0, reward);

team.addPlayer(player);
var fight = new Fight(player,monster);
fight.lifeCycle();