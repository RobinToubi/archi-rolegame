import { Character } from './Character.js';
import { Reward } from './Reward.js';
import { Team } from './Team.js';

export class Player extends Character {
    xp:number;
    items = new Array();
    gold:number;
    history = new Array();
    team: Team;

    constructor(id: number, pv: number, attack: number, pseudo: string, spd: number, team: Team) {
        super();
        this.id = id;
        this.atq = attack;
        this.hp = pv;
        this.pseudo = pseudo;
        this.speed = spd;
        this.alive = true;
        this.xp, this.gold = 0;
        this.team = team || undefined;
    }

    getReward(reward: Reward) {
        this.xp += reward.xp;
        this.items.push(reward.items);
        this.gold += reward.gold;
    }
}