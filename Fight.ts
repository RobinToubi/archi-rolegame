import { Character } from "./Character.js";
import { Monster } from "./Monster.js";
import { Player } from "./Player.js";

export class Fight {
  attacker: Character;
  defender: Character;
  winner: Character;
  loser: Character;

  constructor(att: Character, def: Character) {
    this.defender = def;
    this.attacker = att;
  }

  lifeCycle() {
    let turn = false;
    while (this.winner == undefined) {
        if (turn)
            this.attacker.attack(this.defender);
        else
            this.defender.attack(this.attacker);
        turn = !turn;
    }
  }

  checkWinner() {
    if (this.attacker.hp <= 0) {
      this.winner = this.defender;
      this.loser = this.attacker;
      this.onEnd();
    } else if (this.defender.hp <= 0) {
      this.winner = this.attacker;
      this.loser = this.defender;
      this.onEnd();
    } else this.winner = undefined;
  }

  onEnd() {
    if (this.winner instanceof Player) {
      this.winner.team.getReward((this.loser as Monster).reward);
      console.log(this.winner.pseudo + 'win');
    }
  }
}
