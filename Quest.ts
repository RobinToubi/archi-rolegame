import { Monster } from "./Monster.js";

class Quest {
    name:string;
    monsters: Array<Monster>;

    constructor(name: string, monsters: Monster[]) {
        this.monsters = monsters;
        this.name = name;
    }
}