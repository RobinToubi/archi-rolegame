import { Character } from '../Character.js';
import { Fight } from '../Fight';


class FightController extends Controller {
    attacker: Character;
    defender: Character;


    constructor(view : View,att : Character, def: Character) {
        super(view.path);
        this.attacker = att;
        this.defender = def;
    }

    onInit() {
        super.onInit(__filename);
        var fight = new Fight(this.attacker, this.defender);
        fight.lifeCycle();
    }
}