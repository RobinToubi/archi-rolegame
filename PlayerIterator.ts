import { IPlayerIterator } from "./IPlayerIterator.js";
import { Player } from "./Player.js";

export class PlayerIterator implements IPlayerIterator<Player> {
    private collection: Array<Player>;
    private position:number = 0;

    constructor(collection: Array<Player>) {
        this.collection = collection;
    }

    current(): Player {
        return this.collection[this.position];
    }

    next(): Player {
        return this.collection[this.position];
    }

    key() {
        return this.collection[this.position];
    }

    alive() {
        return true;
    }

    valid() {
        return this.position < this.collection.length;
    }
}